// console.log("Hello ponce");

// [SECTION] Synchronous vs Asynchronous

// Javascript, by default, is synchronous meaning it can only execute a block of code by line

// It reads from  top to bottom and left to right

console.log("Hello World!");
//  console.log("Hello Again");
console.log("Goodbye!");

// [SECTION] Getting All Posts
// A Fetch API allows you to asynchronously request for a resource/data
// "Promise" is an object that represent the eventual completion (or failure) of a asynchronous request
// Syntax:
//  	fetch('URL')

// the connection or the server/url is still available
console.log(fetch("https://jsonplaceholder.typicode.com/posts").then(response => console.log(response.status)));

fetch("https://jsonplaceholder.typicode.com/posts")
//  response -> raw data
// response .json() -> converts data
 .then(response => response.json())

// json => coverted data
.then(json => console.log(json));

// "async" and "await" keywords is another approach to get data
async function fetchData(){

	let result = await fetch("https://jsonplaceholder.typicode.com/posts")

	console.log(result)
	console.log(typeof result)

	let json = await result.json()
	console.log(json)
}

fetchData();

fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => response.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New Post',
		body: "Hello World",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated Post Title",
		body: "Hello Again from ID 1",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: "Corrected Post"
	})
})
.then(response => response.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
})